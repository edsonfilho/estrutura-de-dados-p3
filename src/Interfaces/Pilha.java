package Interfaces;

import Implementacao.Conta;

public interface Pilha {
	
	public void push(Conta conta);
	
	public Conta pop();
	
	public Conta top();
	
	public Integer size();
	
	public void clear();

}
