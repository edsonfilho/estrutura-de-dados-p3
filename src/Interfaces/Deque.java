package Interfaces;

import Implementacao.Conta;

public interface Deque {

	public void insertFront(Conta conta);
	
	public void insertLast(Conta conta);
	
	public void deleteFront();
	
	public void deletLast();
	
	public Conta getFront();
	
	public Conta getLast();
	
	public Integer size();
	
	public boolean isEmpty();
}
