package Interfaces;

import Implementacao.Conta;

public interface Fila {

	public void enque(Conta conta);

	public Object deque();

	public Integer size();

	public Object getFront();

	public Object getReare();

	public boolean isEmpty();

}
