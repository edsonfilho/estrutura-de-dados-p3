package Implementacao;

import Interfaces.Fila;

public class FilaSequencial implements Fila {

    public static final int TAMANHO_INICIAL = 5;
    private Conta[] array = new Conta[TAMANHO_INICIAL];
    private int contador;

    @Override
    public void enque(Conta conta) {
        if (contador == 0) {
            array[0] = conta;
        }
        if (array.length == contador) {
            preencheArray();
        }
        array[contador] = conta;
        contador++;
    }

    @Override
    public Conta deque() {
        Conta temp;
        if (contador == 0) {
            temp = null;
        } else {
            temp = array[contador - 1];
            array[contador - 1] = null;
        }
        contador --;
        return temp;
    }

    @Override
    public Integer size() {
        return contador;
    }

    @Override
    public Conta getFront() {
        return contador == 0 ? null : array[0] ;
    }

    @Override
    public Conta getReare() {
        return contador == 0 ? null : array[contador - 1];
    }

    @Override
    public boolean isEmpty() {
        return contador == 0;
    }

    public Conta[] getArray() {
        return array;
    }

    private void preencheArray() {
        Conta[] arrayTemp = new Conta[TAMANHO_INICIAL * 2];

        for (int i = 0; i <= contador; i++) {
            arrayTemp[i] = array[i];
        }

        array = arrayTemp;
    }
}