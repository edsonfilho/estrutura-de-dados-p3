package Implementacao;

public class Conta {

    private String titular;
    private int contaNumero;
    private Conta proximo;

    public Conta(String titular, int contaNumero) {
        this.titular = titular;
        this.contaNumero = contaNumero;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public int getContaNumero() {
        return contaNumero;
    }

    public void setContaNumero(int contaNumero) {
        this.contaNumero = contaNumero;
    }

    public Conta getProximo() {
        return proximo;
    }

    public void setProximo(Conta proximo) {
        this.proximo = proximo;
    }

    @Override
    public String toString() {
        return "Conta{" +
                "titular='" + titular + '\'' +
                "contaNumero=" + contaNumero + '}';
    }
}

