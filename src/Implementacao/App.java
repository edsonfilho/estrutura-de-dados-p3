package Implementacao;

public class App {

    public static void main(String[] args) {
        FilaSequencial filaSeq = new FilaSequencial();

        Conta conta1 = new Conta("ze1", 1);
        Conta conta2 = new Conta("ze2", 2);
        Conta conta3 = new Conta("ze3", 3);
        Conta conta4 = new Conta("ze4", 4);
        Conta conta5 = new Conta("ze5", 5);


        filaSeq.enque(conta1);
        filaSeq.enque(conta2);
        filaSeq.enque(conta3);
        filaSeq.enque(conta4);
        filaSeq.enque(conta5);

        filaSeq.deque();

    }
}
